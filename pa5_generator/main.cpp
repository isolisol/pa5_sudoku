#include <iostream>
#include "sudoku.h"
using namespace std;

// Main program that runs the Sudoku Game
int main() {
    srand(chrono::system_clock::to_time_t(chrono::system_clock::now()));
    Sudoku *sud = new Sudoku();
    while (!sud->game_over()) {
        sud->print_board();
        char a;
        cin >> a;
        sud->move_pos(a);
    }
    return 0;
}