#include <tuple>
using namespace std;

class Sudoku {
public:
    int board[9][9];
    int position[2];
    int numbers[9];
    Sudoku();
    bool create_puzzle(int board[9][9]);
    void create_box(int x, int y);
    bool validate_square(int row, int col, int value);
    bool grid_full();
    void print_board();
    bool game_over();
    void move_pos(char dir);
};