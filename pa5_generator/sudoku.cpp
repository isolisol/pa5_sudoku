#include <iostream>
#include <random>
#include <algorithm>
#include "sudoku.h"
using namespace std;

// srand(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()));

Sudoku::Sudoku() {
    cout << "inside Sudoku constructor" << endl;
    for (int i = 0; i < 9; i++) {
        for (int j = 0; j < 9; j++) {
            this->board[i][j] = 0;
        }
    }
    this->position[0] = 0;
    this->position[1] = 0;
    this->create_puzzle(this->board);
    for (int i = 1; i < 10; i++) {
        this->numbers[i - 1] = 1;
    }
}

bool Sudoku::validate_square(int row, int col, int value) {
    for (int i = 0; i < 9; i++) {
        if (this->board[row][i] == value) {
            return false;
        }
        if (this->board[i][col] == value) {
            return false;
        }
    }
    int squareX = row / 3;
    int squareY = col / 3;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (this->board[(squareX * 3) + i][(squareY * 3) + j] == value) {
                return false;
            }
        }
    }
    return true;
}

void Sudoku::create_box(int x, int y) {

    // for (int i = 1; i < 10; i++) {
    //     int randomX1 = (rand() % 3) + x;
    //     int randomY1 = (rand() % 3) + y;
    //     cout << "randomX1: " << randomX1 << endl;
    //     cout << "randomY1: " << randomY1 << endl;
    //     // First box
    //     bool check = false;
    //     bool exists = false;
    //     while (!check) {
    //         randomX1 = (rand() % 3) + x;
    //         randomY1 = (rand() % 3) + y;
    //         cout << "randomX1: " << randomX1 << endl;
    //         cout << "randomY1: " << randomY1 << endl;
    //         if (this->board[randomX1][randomY1] == 0) {
    //             for (int j = 0; j < 9; j++) {
    //                 if (this->board[randomX1][j] == i) {
    //                     cout << "in here" << endl;
    //                     exists = true;
    //                     continue;
    //                 }
    //                 cout << "1" << endl;
    //                 if (this->board[j][randomY1] == i) {
    //                     cout << "in here" << endl;
    //                     exists = true;
    //                     continue;
    //                 }
    //                 cout << "out here" << endl;
    //                 cout << "randomX: " << randomX1 << endl;
    //                 cout << "randomY: " << randomY1 << endl;
    //                 cout << "this->board[" << randomX1 <<"][" << j << "]: " << this->board[randomX1][j] << endl;
    //                 cout << "this->board[" << j <<"][" << randomY1 << "]: " << this->board[j][randomY1] << endl;
    //                 cout << "i: " << i << endl;
    //             }
    //             if (!exists) {
    //                 this->board[randomX1][randomY1] = i;
    //                 check = true;
    //             }
    //             exists = false;
    //             this->print_board();
    //             char in;
    //             cin >> in;
    //         }
    //     }
    //     this->print_board();
    // }
}

bool Sudoku::grid_full() {
    for (int i = 0; i < 9; i++) {
        for (int j = 0; j < 9; j++) {
            if (this->board[i][j] == 0) {
                return false;
            }
        }
    }
    return true;
}

bool Sudoku::create_puzzle(int board[9][9]) {
    srand(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()));

    // this->board[0][0] = 1;
    int numbers[9]; // = { 9, 2, 3, 4, 5, 6, 7, 8, 1 };
    for (int i = 0; i < 9; i++) {
        // int life_upper = life_lower + (rand() % (10 - life_lower + 1));
        bool exists = false;
        int num = 1 + (rand() % 9);
        // int num = rand() % 10;
        for (int j = 0; j < 9; j++) {
            if (num == numbers[j]) {
                exists = true;
                break;
            }
        }
        if (!exists) {
            numbers[i] = num;
        } else {
            i--;
        }
    }

    int x;
    int y;
    for (int i = 0; i < 81; i++) {
        x = i / 9;
        y = i % 9;
        if (this->board[x][y] == 0) {
            // random_shuffle(&numbers[0], &numbers[9]);
            // random_shuffle(begin(numbers), end(numbers));
            for (int j = 0; j < 9; j++) {
                int value = numbers[j];
                if (this->validate_square(x, y, value)) {
                    this->board[x][y] = value;
                    this->print_board();
                    if (grid_full()) {
                        return true;
                    } else {
                        // char a;
                        // cin >> a;
                        bool lala = this->create_puzzle(this->board);
                        if (lala) {
                            return true;
                        }
                    }
                }
            }
            break;
        }
    }
    this->board[x][y] = 0;
    return false;
}

void Sudoku::print_board() {
    int x = this->position[0];
    int y = this->position[1];
    cout << "x: " << x << ", y: " << y << endl;
    cout << " -----------------------------------------------------" << endl;
    cout << "|                 |                 |                 |" << endl;
    for (int i = 0; i < 9; i++) {
        cout << "| ";
        for (int j = 0; j < 9; j++) {
            if (i == x && j == y) {
                cout << "> ";
            } else {
                cout << "  ";
            }
            if (this->board[i][j] == 0) {
                cout << "?  ";
            } else {
                cout << this->board[i][j] << "  ";
            }
            if ((j + 1) % 3 == 0) {
                cout << " | ";
            }
        }
        cout << "\n|                 |                 |                 |";
        if ((i + 1) % 3 == 0) {
            cout << "\n -----------------------------------------------------";
            if (i != 8) {
                cout << "\n|                 |                 |                 |";
            }
        }
        cout << endl;
    }
}

bool Sudoku::game_over() {
    return false;
}

void Sudoku::move_pos(char dir) {
    if (dir == 'r') {
        // move position one to the right
        if (this->position[1] != 8) {
            this->position[1]++;
        }
    } else if (dir == 'l') {
        // move position one to the left
        if (this->position[1] != 0) {
            this->position[1]--;
        }
    } else if (dir == 'u') {
        // move position one up
        if (this->position[0] != 0) {
            this->position[0]--;
        }
    } else if (dir == 'd') {
        // move position one down
        if (this->position[0] != 8) {
            this->position[0]++;
        }
    }
}