#include <fstream>
using namespace std;

class Sudoku {
public:
    int board[9][9];
    int solution[9][9];
    int position[2];
    Sudoku(ifstream &in);
    void reveal_numbers();
    void next_pos();
    void print_board();
    void print_solution();
    void move_pos(char dir);
};