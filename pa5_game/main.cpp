#include <iostream>
#include <string>
#include <random>
#include "sudoku.h"

void move_pos(Sudoku *s) {
    char dir;
    while (dir != 'x') {
        cout << "input r for right, l for left, u for up, d for down and x to quit" << endl;
        cout << "direction: ";
        cin >> dir;
        s->move_pos(dir);
        s->print_board();
    }
}

int main() {
    srand(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()));
    ifstream in1("game_bank/count.txt");
    int count;
    in1 >> count;
    in1.close();
    int random = rand() % count;
    cout << "count: " << count << endl;
    string filename = "game_bank/sudoku" + to_string(random) + ".txt";
    ifstream in(filename);
    Sudoku *s = new Sudoku(in);
    in.close();
    while (true) {
        s->print_board();
        char user_input;
        cout << "1. Move position" << endl;
        cout << "2. Input number" << endl;
        cout << "Choice: ";
        cin >> user_input;
        if (user_input == '1') {
            move_pos(s);
        }
        // cout << "move position (l, r, u, d): ";
        // cin >> dir;
        // s->move_pos(dir);
    }
    return 0;
}