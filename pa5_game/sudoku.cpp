#include <iostream>
#include "sudoku.h"
using namespace std;

Sudoku::Sudoku(ifstream &in) {
    // this->position[0] = 0;
    // this->position[1] = 0;
    int buffer;
    int row = 0;
    for (int i = 0; i < 81; i++) {
        in >> buffer;
        this->solution[row][i % 9] = buffer;
        if ((i + 1) % 9 == 0) {
            row++;
        }
    }
    this->reveal_numbers();
    this->next_pos();
}

void Sudoku::next_pos() {
    int row = this->position[0];
    int col = this->position[1];
    bool pos_found = false;
    for (int i = (row * 9) + col; i < 81; i++) {
        if (this->board[i / 9][i % 9] == 0) {
            this->position[0] = i / 9;
            this->position[1] = i % 9;
            pos_found = true;
            break;
        }
    }
    if (!pos_found) {
        for (int i = 0; i < (row * 9) + col; i++) {
            if (this->board[i / 9][i % 9] == 0) {
            this->position[0] = i / 9;
            this->position[1] = i % 9;
            }
        }
    }
}

void Sudoku::reveal_numbers() {
    // Reveals two squares in each box and then reveals an extra 17 squares.
    int counter = 0;
    int boxes[9][9] = {
        { 0, 1, 2, 9, 10, 11, 18, 19, 20 },
        { 3, 4, 5, 12, 13, 14, 21, 22, 23 },
        { 6, 7, 8, 15, 16, 17, 24, 25, 26 }, 
        { 27, 28, 29, 36, 37, 38, 45, 46, 47 },
        { 30, 31, 32, 39, 40, 41, 48, 49, 50 },
        { 33, 34, 35, 42, 43, 44, 51, 52, 53 },
        { 54, 55, 56, 63, 64, 65, 72, 73, 74 },
        { 57, 58, 59, 66, 67, 68, 75, 76, 77 },
        { 60, 61, 62, 69, 70, 71, 78, 79, 80 }
    };
    int random_sqaures[18];
    for (int i = 0; i < 9; i++) {
        int random_index1 = rand() % 9;
        int random_index2 = rand() % 9;
        while (random_index1 == random_index2) {
            random_index2 = rand() % 9;
        }
        int random_number1 = boxes[i][random_index1];
        int random_number2 = boxes[i][random_index2];
        this->board[random_number1 / 9][random_number1 % 9] = this->solution[random_number1 / 9][random_number1 % 9];
        this->board[random_number2 / 9][random_number2 % 9] = this->solution[random_number2 / 9][random_number2 % 9];
    }
    while (counter != 17) {
        int num = rand() % 81;
        int row = num / 9;
        int col = num % 9;
        if (this->board[row][col] == 0) {
            this->board[row][col] = this->solution[row][col];
            counter++;
        }
    }
}

void Sudoku::print_board() {
    int x = this->position[0];
    int y = this->position[1];
    cout << " -----------------------------------------------------" << endl;
    cout << "|                 |                 |                 |" << endl;
    for (int i = 0; i < 9; i++) {
        cout << "| ";
        for (int j = 0; j < 9; j++) {
            if (i == x && j == y) {
                cout << "> ";
            } else {
                cout << "  ";
            }
            if (this->board[i][j] == 0) {
                cout << "*  ";
            } else {
                cout << this->board[i][j] << "  ";
            }
            if ((j + 1) % 3 == 0) {
                cout << " | ";
            }
        }
        cout << "\n|                 |                 |                 |";
        if ((i + 1) % 3 == 0) {
            cout << "\n -----------------------------------------------------";
            if (i != 8) {
                cout << "\n|                 |                 |                 |";
            }
        }
        cout << endl;
    }
}

// void Sudoku::move_pos(char dir) {
//     if (dir == 'r') {
//         // move position one to the right
//         if (this->position[1] != 8) {
//             this->position[1]++;
//         }
//     } else if (dir == 'l') {
//         // move position one to the left
//         if (this->position[1] != 0) {
//             this->position[1]--;
//         }
//     } else if (dir == 'u') {
//         // move position one up
//         if (this->position[0] != 0) {
//             this->position[0]--;
//         }
//     } else if (dir == 'd') {
//         // move position one down
//         if (this->position[0] != 8) {
//             this->position[0]++;
//         }
//     }
// }

void Sudoku::move_pos(char dir) {
    if (dir == 'r') {
        int old_pos = this->position[1];
        this->position[1] = (old_pos + 1) % 9;
        while (this->board[this->position[0]][this->position[1]] != 0) {
            old_pos = this->position[1];
            this->position[1] = (old_pos + 1) % 9;
        }
    } else if (dir == 'l') {
        int old_pos = this->position[1];
        if (old_pos == 0) {
            old_pos = 8;
        }
        this->position[1] = old_pos - 1;
        while (this->board[this->position[0]][this->position[1]] != 0) {
            old_pos = this->position[1];
            if (old_pos == 0) {
                old_pos = 8;
            }
            this->position[1] = old_pos - 1;
        }
    } else if (dir == 'u') {
        int old_pos = this->position[0];
        if (old_pos == 0) {
            this->position[0] = 8;
        }
        while (this->board[this->position[0]][this->position[1]] != 9) {
            old_pos = this->position[0];
            // if ()
        }
    } else if (dir == 'd') {
        int old_pos = this->position[0];
        this->position[0] = (old_pos + 1) % 9;
        while (this->board[this->position[0]][this->position[1]] != 0) {
            old_pos = this->position[0];
            this->position[0] = (old_pos + 1) % 9;
        }
    }
}